
# Docker Guacamole + nginx reverse proxy
**Disclaimer:** This work is based on the work of: https://github.com/oznu/docker-guacamole
A Docker Container for [Apache Guacamole](https://guacamole.apache.org/), a client-less remote desktop gateway. It supports standard protocols like VNC, RDP, and SSH over HTML5.
This container runs the guacamole web client, the guacd server and a postgres database.
https://github.com/MaxWaldorf/guacamole.git

## Usage (works for x86_64 and arm64v8, no support for 32 bits)

```shell
#產生ssl憑證
./prepare.sh
#啟動
docker-compose up -d
#關掉
docker-compose down

```

之後到https://yourip:8443/yourapp/就可以用自己在user-mapping.xml中設定的帳號登入.

## Default User
The default username is `guacadmin` with password `guacadmin`.

## License
(Based on OZNU choice GPLv3)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the [GNU General Public License](./LICENSE) for more details.

